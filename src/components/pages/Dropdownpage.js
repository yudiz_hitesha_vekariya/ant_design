import React from "react";
import { Menu, Dropdown } from "antd";
import { Rate } from "antd";
import { FrownOutlined, MehOutlined, SmileOutlined } from "@ant-design/icons";

const Dropdownpage = () => {
  const menu = (
    <Menu
      style={{
        width: "200%",
        padding: "20px",
      }}
    >
      <Menu.Item
        key="1"
        style={{
          textAlign: "center",
          fontSize: "18px",
        }}
      >
        {" "}
        hitesha{" "}
      </Menu.Item>
      <Menu.Item
        key="2"
        style={{
          textAlign: "center",
          fontSize: "18px",
        }}
      >
        niharika
      </Menu.Item>
      <Menu.Item
        key="3"
        style={{
          textAlign: "center",
          fontSize: "18px",
        }}
      >
        anjali
      </Menu.Item>
      <Menu.Item
        key="3"
        style={{
          textAlign: "center",
          fontSize: "18px",
        }}
      >
        sneha
      </Menu.Item>
    </Menu>
  );
  const customIcons = {
    1: <FrownOutlined />,
    2: <FrownOutlined />,
    3: <MehOutlined />,
    4: <SmileOutlined />,
    5: <SmileOutlined />,
  };
  return (
    <div>
      <div>
        <Dropdown overlay={menu} trigger={["contextMenu"]}>
          <div
            className="site-dropdown-context-menu"
            style={{
              textAlign: "center",
              height: 200,
              lineHeight: "200px",
              marginTop: "10%",
              width: "50%",
              marginLeft: "320px",
              backgroundColor: "rgb(196, 189, 189)",
              boxShadow:
                "rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px",
              fontSize: "20px",
            }}
          >
            Right Click on here
          </div>
        </Dropdown>
      </div>
      <br></br>
      <br></br>
      <Rate defaultValue={2} character={({ index }) => index + 1} />
      <br />
      <Rate
        defaultValue={3}
        character={({ index }) => customIcons[index + 1]}
      />
    </div>
  );
};

export default Dropdownpage;
