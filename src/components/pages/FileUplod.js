import React from "react";
import { Button, Spin, Upload } from "antd";

const FileUplod = () => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <Upload.Dragger
        multiple={true}
        listType="picture"
        showUploadList={{ showRemoveIcon: true }}
        accept=".png,.jpeg,.jpg,.doc"
        action={"http://localhost:3000/"}
        defaultFileList={[
          {
            uid: "abc",
            name: "exising_file.png",
            status: "uploading",
            percent: 50,
            url: "https://www.google.com/",
          },
        ]}
        iconRender={() => {
          return <Spin></Spin>;
        }}
        progress={{
          strokeWidth: 3,
          strokeColor: {
            "0%": "#f0f",
            "100%": "#ff0",
          },
        }}
      >
        Drag Files here OR
        <br />
        <Button>Click Upload</Button>
      </Upload.Dragger>
    </div>
  );
};

export default FileUplod;
