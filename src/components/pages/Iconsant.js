import React from "react";
import { Space } from "antd";
import {
  SmileTwoTone,
  HeartTwoTone,
  CheckCircleTwoTone,
  GithubFilled,
  SkypeFilled,
  AppleFilled,
  SnippetsOutlined,
  TwitterOutlined,
  LoadingOutlined,
  CrownTwoTone,
  CustomerServiceFilled,
  WhatsAppOutlined,
} from "@ant-design/icons";

const Iconsant = () => {
  return (
    <div
      style={{
        marginTop: "12%",
        border: "2px solid white",
        padding: "50px",
        width: "50%",
        marginLeft: "325px",
        boxShadow:
          "rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px",
      }}
    >
      <Space>
        <SmileTwoTone />
        <h1
          style={{
            color: "GrayText",
          }}
        >
          SMILEY
        </h1>
        <br></br>
        <TwitterOutlined />
        <h1
          style={{
            color: "GrayText",
          }}
        >
          TWITTER
        </h1>
        <br></br>
        <CheckCircleTwoTone twoToneColor="#52c41a" />
        <h1
          style={{
            color: "GrayText",
          }}
        >
          CHECKCIRCLE
        </h1>
      </Space>
      <br></br>
      <br></br>
      <Space>
        <GithubFilled />
        <h1
          style={{
            color: "GrayText",
          }}
        >
          GITHUB
        </h1>
        <br></br>
        <SkypeFilled />
        <h1
          style={{
            color: "GrayText",
          }}
        >
          SKYPE
        </h1>
        <br></br>
        <AppleFilled />
        <h1
          style={{
            color: "GrayText",
          }}
        >
          APPLE
        </h1>
      </Space>
      <br></br>
      <br></br>
      <Space>
        <SnippetsOutlined />
        <HeartTwoTone twoToneColor="#eb2f96" />
        <LoadingOutlined />
        <CrownTwoTone />
        <CustomerServiceFilled />
        <WhatsAppOutlined />
      </Space>
    </div>
  );
};

export default Iconsant;
