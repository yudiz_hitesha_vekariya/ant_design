import React from "react";
import { Steps, Button, message } from "antd";
import "./step.css";
const { Step } = Steps;

const steps = [
  {
    title: "niharika",
    content:
      "Find a people who challenge and inspire you; spend a lot of time with them, and it will change your life.",
  },
  {
    title: "hitesha",
    content:
      "There is nothing on this earth more to be prized than  friendship.",
  },
  {
    title: "anjali",
    content:
      "There’s nothing better than a friend, unless it is a friend with chocolate.",
  },
];

const Steppage = () => {
  const [current, setCurrent] = React.useState(0);

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  return (
    <div
      style={{
        marginTop: "3%",
      }}
    >
      <>
        <Steps current={current}>
          {steps.map((item) => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
        <div className="steps-content">
          {steps[current].content}
          <img
            src="https://i.pinimg.com/originals/93/cb/18/93cb180fca5ef9c6b5ed79d27752f965.gif"
            alt=""
          ></img>
        </div>
        <div className="steps-action">
          {current < steps.length - 1 && (
            <Button type="primary" onClick={() => next()}>
              Next
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button
              type="primary"
              onClick={() => message.success("Processing complete!")}
            >
              Done
            </Button>
          )}
          {current > 0 && (
            <Button style={{ margin: "0 8px" }} onClick={() => prev()}>
              Previous
            </Button>
          )}
        </div>
      </>
    </div>
  );
};

export default Steppage;
