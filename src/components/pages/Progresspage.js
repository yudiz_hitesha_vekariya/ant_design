import React from "react";
import { Button, DatePicker, Input, Progress, Slider, Space, Spin } from "antd";

const Progresspage = () => {
  const handleClick = () => {
    alert("thank you !!");
  };
  return (
    <div
      style={{
        marginTop: "10%",
      }}
    >
      <Space direction="vertical">
        <Button type="primary" onClick={handleClick}>
          Button
        </Button>
        <Input placeholder="type here..."></Input>
        <Progress percent={50} type="circle"></Progress>
        <Spin spinning />
        <DatePicker />
        <Slider />
      </Space>
    </div>
  );
};

export default Progresspage;
