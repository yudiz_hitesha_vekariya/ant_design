import React, { useState } from "react";
import { Drawer, Button } from "antd";
import { notification, Space } from "antd";

const Drawerpage = () => {
  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };
  const openNotificationWithIcon = (type) => {
    notification[type]({
      message: "Notification Title",
      description:
        "This is the content of the notification. This is the content of the notification. This is the content of the notification.",
    });
  };
  return (
    <>
      <Space
        style={{
          marginTop: "6%",
        }}
      >
        <Button onClick={() => openNotificationWithIcon("success")}>
          Success
        </Button>
        <Button onClick={() => openNotificationWithIcon("info")}>Info</Button>
        <Button onClick={() => openNotificationWithIcon("warning")}>
          Warning
        </Button>
        <Button onClick={() => openNotificationWithIcon("error")}>Error</Button>
      </Space>
      <br></br>
      <br></br>
      <br></br>
      <Button
        style={{
          marginTop: "15%",
        }}
        type="primary"
        onClick={showDrawer}
      >
        Open
      </Button>
      <Drawer
        title="Basic Drawer"
        placement="right"
        onClose={onClose}
        visible={visible}
      >
        <p>Some contents...</p>
        <br></br>
        <br></br>
        <p>Some contents...</p>
        <br></br>
        <br></br>
        <p>Some contents...</p>
      </Drawer>
    </>
  );
};
export default Drawerpage;
