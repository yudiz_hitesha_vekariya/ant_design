import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import FileUplod from "../pages/FileUplod";
import NavBar from "../navbar/NavBar";
import Home from "../pages/Home";
import Iconsant from "../pages/Iconsant";
import Dropdownpage from "../pages/Dropdownpage";
import Steppage from "../pages/step";
import Calenderpage from "../pages/Calenderpage";
import Drawerpage from "../pages/Drawerpage";
import Progresspage from "../pages/Progresspage";

const Routing = () => {
  return (
    <BrowserRouter>
      <NavBar />
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/upload" element={<FileUplod />} />
        <Route exact path="/icon" element={<Iconsant />} />
        <Route exact path="/drop" element={<Dropdownpage />} />
        <Route exact path="/step" element={<Steppage />} />
        <Route exact path="/cal" element={<Calenderpage />} />
        <Route exact path="/drawer" element={<Drawerpage />} />
        <Route exact path="/progress" element={<Progresspage />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Routing;
