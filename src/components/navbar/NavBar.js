import React from "react";
import { NavLink } from "react-router-dom";
import "./navbar.css";

const NavBar = () => {
  return (
    <>
      <header>
        <nav>
          <div className="header-title">
            <NavLink to="/"> ANT DESIGN </NavLink>
          </div>
          <div className="navbar-items">
            <div className="dropdown">
              <div className="dropbtn">Topics</div>
              <div className="dropdown-content">
                <NavLink to="/upload"> FileUplod </NavLink>
                <NavLink to="/step"> Steps </NavLink>
                <NavLink to="/progress"> Progress </NavLink>
                <NavLink to="/icon"> icons </NavLink>
                <NavLink to="/drop"> DropDown with Rate</NavLink>
                <NavLink to="/drawer"> Drawer and notification</NavLink>
                <NavLink to="/cal"> Calender </NavLink>
              </div>
            </div>
          </div>
        </nav>
      </header>
    </>
  );
};

export default NavBar;
