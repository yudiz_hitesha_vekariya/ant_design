import "./App.css";
import Routing from "./components/route/Routing";

function App() {
  return (
    <div className="App">
      <Routing />
    </div>
  );
}

export default App;
